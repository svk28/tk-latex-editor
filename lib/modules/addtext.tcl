######################################################
#                TkTeXEditor
#        Distributed under GNU Public License
# Author: Sergey Kalinin (BanZaj) banzaj@lrn.ru
# Copyright (c) "CONERO lab", 2002, http://conero.lrn.ru
######################################################

proc AddText {action} {
    global dir font color nb
    set node [$nb raise]
    if {$node == "" || $node == "debug"} {return}
    set text "$nb.f$node.f.text"
    set pos [$text index insert]
    set curLine [lindex [split $pos "."] 0]
    set cursor [lindex [split $pos "."] 1]
    set editLine [$text get $curLine.0 $pos]
    set types {
        {"EPS" {.eps}}
        {"PDF" {.pdf}}
        {"PNG" {.png}}
        {"JPG" {.jpg}}
        {"GIF" {.gif}}
    }
    set selIndex [$text tag ranges sel]
    if {$selIndex != ""} {
        set posBegin [lindex $selIndex 0]
        set posEnd [lindex $selIndex 1]
        set curLine [string range $posBegin 0 [expr [string first "\." $posBegin] - 1]]
        set cursor [string range $posBegin [expr [string first "\." $posBegin] + 1] end]
        set endLine [string range $posEnd 0 [expr [string first "\." $posEnd] - 1]]
        set endCur [string range $posEnd [expr [string first "\." $posBegin] + 1] end]
        set selText [$text get $posBegin $posEnd]
    } else {
        set posBegin "$curLine.$cursor"
        set endLine [expr $curLine +1]
        set endCur 0
    }
    switch -- $action {
        enumerate {
            #insert - 1 chars wordstart" "insert wordend - 1 chars"
            $text insert $posBegin "\\begin\{enumerate\}\n"
            $text insert [expr $curLine +1].0 "\\item \n"
            $text insert [expr $curLine +2].0 "\\end\{enumerate\}"
            set t [$text get $posBegin $curLine.end]
            set curPos "[expr $curLine + 1]\.end"            
        }
        image {
            set file [tk_getOpenFile -initialdir $dir(doc) -filetypes $types -parent .\
            -title "[::msgcat::mc "Select image"]"]
            #set fileName [file tail $file]
            if {$file == ""} {
                return
            }
            $text insert $posBegin "\\includegraphics\[\]\{$file\}\n"
            set t [$text get $posBegin $curLine.end]
            set curPos "$curLine\.[string last "\\" $t]"
        }    
        itemize {
            $text insert $posBegin "\\begin\{itemize\}\n"
            $text insert [expr $curLine +1].0 "\\item \n"
            $text insert [expr $curLine +2].0 "\\end\{itemize\}"
            set t [$text get $posBegin $curLine.end]
            set curPos "[expr $curLine +1]\.end"
        }
        item {
            set editLine [$text get [expr $curLine - 1].0 $pos]                
            if [regexp -nocase -all -- {(\\item)} $editLine match v1] {
                $text insert $curLine.0 "\\item "
                set curPos "$curLine\.6"
            } else {
                return
            }
        }
        date {
            set editLine [$text get [expr $curLine - 1].0 $pos]
            set unixTime [exec date +%s]
            set dateTime [clock format $unixTime -format "%d.%m.%Y, %H:%M:%S"]
            $text insert $curLine.0 "$dateTime"
            set curPos "$curLine\.end"
        }
    }
    $text mark set insert $curPos
    $text see insert
}

######################################################

proc AddTextFont {action} {
    global dir font color nb
    set node [$nb raise]
    if {$node == "" || $node == "debug"} {return}
    set text "$nb.f$node.f.text"
    set pos [$text index insert]
    set curLine [lindex [split $pos "."] 0]
    set cursor [lindex [split $pos "."] 1]
    set editLine [$text get $curLine.0 $pos]
    set selIndex [$text tag ranges sel]
    if {$selIndex != ""} {
        set posBegin [lindex $selIndex 0]
        set posEnd [lindex $selIndex 1]
        set curLine [string range $posBegin 0 [expr [string first "\." $posBegin] - 1]]
        set cursor [string range $posBegin [expr [string first "\." $posBegin] + 1] end]
        set endLine [string range $posEnd 0 [expr [string first "\." $posEnd] - 1]]
        set endCur [string range $posEnd [expr [string first "\." $posBegin] + 1] end]
        set selText [$text get $posBegin $posEnd]
        $text insert $endLine.$endCur "\}"
        $text insert $posBegin "\\$action\{"
        set t [$text get $posBegin $curLine.end]
        set curPos "$endLine\.end"
        $text mark set insert $curPos
    } else {
        set posBegin "$curLine.$cursor"
        set endLine [expr $curLine +0]
        $text insert $posBegin "\\$action\{\}"
        $text mark set insert "$endLine.end - 1 chars"
    }
    $text see insert
}

proc AddTextCommand {action} {
    global dir font color nb
    set node [$nb raise]
    if {$node == "" || $node == "debug"} {return}
    set text "$nb.f$node.f.text"
    set pos [$text index insert]
    set curLine [lindex [split $pos "."] 0]
    set cursor [lindex [split $pos "."] 1]
    set editLine [$text get $curLine.0 $pos]
    set selIndex [$text tag ranges sel]
    if {$selIndex != ""} {
        set posBegin [lindex $selIndex 0]
        set posEnd [lindex $selIndex 1]
        set curLine [string range $posBegin 0 [expr [string first "\." $posBegin] - 1]]
        set cursor [string range $posBegin [expr [string first "\." $posBegin] + 1] end]
        set endLine [string range $posEnd 0 [expr [string first "\." $posEnd] - 1]]
        set endCur [string range $posEnd [expr [string first "\." $posBegin] + 1] end]
        set selText [$text get $posBegin $posEnd]
        $text insert $posBegin "\\$action "
        set t [$text get $posBegin $curLine.end]
        set curPos "$endLine\.end"
        $text mark set insert $curPos
    } else {
        set posBegin "$curLine.$cursor"
        set endLine [expr $curLine +0]
        $text insert $posBegin "\\$action "
        $text mark set insert "$endLine.end"
    }
    $text see insert
}

proc AddTextDouble {beginText endText} {
    global dir font color nb
    set node [$nb raise]
    if {$node == "" || $node == "debug"} {return}
    set text "$nb.f$node.f.text"
    set pos [$text index insert]
    set curLine [lindex [split $pos "."] 0]
    set cursor [lindex [split $pos "."] 1]
    set editLine [$text get $curLine.0 $pos]
    set selIndex [$text tag ranges sel]
    
    if {$selIndex != ""} {
        set posBegin [lindex $selIndex 0]
        set posEnd [lindex $selIndex 1]
        set curLine [string range $posBegin 0 [expr [string first "\." $posBegin] - 1]]
        set cursor [string range $posBegin [expr [string first "\." $posBegin] + 1] end]
        set endLine [string range $posEnd 0 [expr [string first "\." $posEnd] - 1]]
        set endCur [string range $posEnd [expr [string first "\." $posBegin] + 1] end]
        set selText [$text get $posBegin $posEnd]
        if {[string range $endText 0 1] == "\n"} {
            $text insert $endLine.$endCur " \\$endText"
        } else {
            $text insert $endLine.$endCur "\n\\[string trim $endText "\n"]"
        }
        $text insert $posBegin "\\$beginText "
    } else {
        set posBegin "$curLine.$cursor"
        if {[string range $endText 0 1] == "\n"} {
            $text insert $posBegin "\\$beginText  \\$endText"
        } else {
            $text insert $posBegin "\\$beginText  \n\\[string trim $endText "\n"]"
            puts "$beginText  $endText"
        }
    }
    
    set ind [expr [string length $beginText] + 2]
    set curPos "$posBegin + $ind chars"
    $text mark set insert $curPos
    $text see insert
}

proc AddTextMath {beginText endText} {
    global dir font color nb
    set node [$nb raise]
    if {$node == "" || $node == "debug"} {return}
    set text "$nb.f$node.f.text"
    set pos [$text index insert]
    set curLine [lindex [split $pos "."] 0]
    set cursor [lindex [split $pos "."] 1]
    set editLine [$text get $curLine.0 $pos]
    set selIndex [$text tag ranges sel]
    
    if {$selIndex != ""} {
        set posBegin [lindex $selIndex 0]
        set posEnd [lindex $selIndex 1]
        set curLine [string range $posBegin 0 [expr [string first "\." $posBegin] - 1]]
        set cursor [string range $posBegin [expr [string first "\." $posBegin] + 1] end]
        set endLine [string range $posEnd 0 [expr [string first "\." $posEnd] - 1]]
        set endCur [string range $posEnd [expr [string first "\." $posBegin] + 1] end]
        set selText [$text get $posBegin $posEnd]
        if {[string range $endText 0 1] == "\n"} {
            $text insert $endLine.$endCur " $endText"
        } else {
            $text insert $endLine.$endCur "[string trim $endText "\n"]"
        }
        $text insert $posBegin "$beginText "
    } else {
        set posBegin "$curLine.$cursor"
        if {[string range $endText 0 1] == "\n"} {
            $text insert $posBegin "$beginText  \\$endText"
        } else {
            $text insert $posBegin "$beginText  [string trim $endText "\n"]"
        }
    }
    set ind [expr [string length $beginText] + 2]
    set curPos "$posBegin + $ind chars"
    $text mark set insert $curPos
    $text see insert
}





