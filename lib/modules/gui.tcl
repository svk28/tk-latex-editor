######################################################
#                TkTeXeditor
#        Distributed under GNU Public License
# Author: Sergey Kalinin (BanZaj) banzaj@lrn.ru
# Copyright (c) "CONERO lrn", 2000, http//conero.lrn.ru
######################################################

######################################################
#                                                    #
#           Programms main interface window          #
#                                                    #
######################################################


## MAIN INTERFACE WINDOW ##
wm geometry . $gui(geometry)
set activeFile ""
wm title . "TkLaTeXEditor $ver$release"
wm iconname . "TkTeXEditor $ver$release"
wm protocol . WM_DELETE_WINDOW Quit
wm overrideredirect . 0
wm positionfrom . user
#wm resizable . 0 0

## main frames
frame .frmMenu -border 1 -relief raised -background $color(bg)
frame .frmTool -border 1 -relief raised -background $color(bg)
frame .frmToolEdit -border 1 -relief raised -background $color(bg)
frame .frmBody -border 1 -relief raised -background $color(bg)
frame .frmStatus -border 1 -relief sunken -background $color(bg)
pack .frmMenu -side top -padx 1 -fill x
pack .frmTool -side top -padx 1 -fill x
pack .frmToolEdit -side top -padx 1 -fill x
pack .frmBody -side top -padx 1 -fill both -expand true
pack .frmStatus -side top -padx 1 -fill x

## MENU ##
menubutton .frmMenu.mnuFile -text [::msgcat::mc "File"] -menu .frmMenu.mnuFile.m -font $font(normal) -background $color(bg)
set m [menu .frmMenu.mnuFile.m -background $color(bg)]
$m add cascade -label [::msgcat::mc "New"] -menu $m.new -font $font(normal)
set mn [menu $m.new -background $color(bg)]
$mn add command -label [::msgcat::mc "New file"] -command {FileDialog new}\
-font $font(normal) -accelerator "Ctrl+N"
$mn add command -label [::msgcat::mc "Create from template"] -command {FileDialog new_templ}\
-font $font(normal)
$mn add command -label [::msgcat::mc "New project"] -command {NewProj add ""}\
-font $font(normal) -state disable
$m add command -label [::msgcat::mc "Open"] -command {FileDialog open}\
-font $font(normal) -accelerator "Ctrl+O"
$m add command -label [::msgcat::mc "Save"] -command {FileDialog save}\
-font $font(normal) -accelerator "Ctrl+S"
$m add command -label [::msgcat::mc "Save as"] -command {FileDialog save_as}\
-font $font(normal) -accelerator "Ctrl+A"
$m add command -label [::msgcat::mc "Save all"] -command {FileDialog save_all}\
-font $font(normal)
$m add command -label [::msgcat::mc "Close"] -command {FileDialog close}\
-font $font(normal) -accelerator "Ctrl+W"
$m add command -label [::msgcat::mc "Close all"] -command {FileDialog close_all}\
-font $font(normal)
$m add command -label [::msgcat::mc "Delete"] -command {FileDialog delete}\
-font $font(normal) -accelerator "Ctrl+D"
$m add separator
$m add command -label [::msgcat::mc "Print"] -command {PrintDialog print}\
-font $font(normal) -accelerator "Ctrl+P" -state normal
#$m add separator
#$m add command -label [::msgcat::mc "Settings"] -command Settings -font $font(normal)
$m add separator
$m add command -label [::msgcat::mc "Exit"] -command Quit -font $font(normal) -accelerator "Ctrl+Q"

### DOCUMENT MENU ####
menubutton .frmMenu.mnuDoc -text [::msgcat::mc "Document"] -menu .frmMenu.mnuDoc.m -font $font(normal) -background $color(bg)

set m [menu .frmMenu.mnuDoc.m -background $color(bg)]
#$m add command -label [::msgcat::mc "Included packages"] -command {PackageDialog}\
#-font $font(normal) -state disable
# "convert to" submenu ##
$m add cascade -label "[::msgcat::mc "Convert to"] ..." -menu $m.convert -font $font(normal) -state normal
set mn [menu $m.convert -background $color(bg)]
$mn add command -label [::msgcat::mc "DVI"] -font $font(normal) -accelerator "F5" -state normal\
-command {ConvertDialog dvi}
$mn add command -label [::msgcat::mc "PS"] -font $font(normal) -accelerator "F6" -state normal\
-command {ConvertDialog ps}
$mn add command -label [::msgcat::mc "PDF"] -font $font(normal) -accelerator "F7" -state normal\
-command {ConvertDialog pdf}
$mn add command -label "HTML" -font $font(normal) -command {ConvertDialog html}\

$mn add separator
## "preview" submenu ##
$m add cascade -label [::msgcat::mc "Preview"] -menu $m.preview -font $font(normal) -state normal
set mn [menu $m.preview -background $color(bg)]
$mn add command -label "DVI" -command {Preview dvi} -font $font(normal)
$mn add command -label "PS" -command {Preview ps} -font $font(normal)
$mn add command -label "PDF" -command {Preview pdf} -font $font(normal)
$mn add command -label "HTML" -command {Preview html} -font $font(normal)

$m add separator
$m add command -label [::msgcat::mc "Spell check"] -font $font(normal) -state normal\
-command {
    SpellCheck $nb.f[$nb raise].f.text
}
$m add command -label [::msgcat::mc "Encoding"] -font $font(normal) -state normal\
-command {EncodeDialog file}

$m add command -label [::msgcat::mc "Syntax check"] -command {DoModule syntax}\
-font $font(normal) -state disable

##.frmMenu 'EDIT' ##
menubutton .frmMenu.mnuEdit -text [::msgcat::mc "Edit"] -menu .frmMenu.mnuEdit.m -font $font(normal) -background $color(bg)
set m [menu .frmMenu.mnuEdit.m -background $color(bg)]
$m add command -label [::msgcat::mc "Undo"] -font $font(normal) -accelerator "Ctrl+Z"\
-state normal -command "CutPast undo"
$m add separator
$m add command -label [::msgcat::mc "Copy"] -font $font(normal) -accelerator "Ctrl+C"\
-command "CutPast copy"
$m add command -label [::msgcat::mc "Paste"] -font $font(normal) -accelerator "Ctrl+V"\
-command "CutPast paste"
$m add command -label [::msgcat::mc "Cut"] -font $font(normal) -accelerator "Ctrl+X"\
-command "CutPast cut"
$m add separator
$m add command -label [::msgcat::mc "Select All"] -command {Select all} -font $font(normal) -accelerator "Ctrl+/"
$m add command -label [::msgcat::mc "Remove selection"] -command {Select remove} -font $font(normal)\
-accelerator "Ctrl+\\"
$m add separator
$m add command -label [::msgcat::mc "Goto line"] -command GoToLine -font $font(normal)\
-accelerator "Ctrl+G"
$m add command -label [::msgcat::mc "Find"] -command Find -font $font(normal) -accelerator "Ctrl+F"
$m add command -label [::msgcat::mc "Replace"] -command ReplaceDialog -font $font(normal)\
-accelerator "Ctrl+R"
$m add separator
$m add cascade -label "[::msgcat::mc "Custom operations"] ..." -menu $m.hotkeys -font $font(normal) -state normal
menu $m.hotkeys -background $color(bg)

## MENU for selected text ##
set m [menu .frmMenu.mnuEdit.m.selection -background $color(bg)]
$m add command -label [::msgcat::mc "Select All"] -command {Select all} -font $font(normal) -accelerator "Ctrl+/"
$m add command -label [::msgcat::mc "Remove selection"] -command {Select remove} -font $font(normal)\
-accelerator "Ctrl+\\"
$m add separator
$m add command -label [::msgcat::mc "Copy"] -font $font(normal) -accelerator "Ctrl+C"\
-command "CutPast copy"
$m add command -label [::msgcat::mc "Paste"] -font $font(normal) -accelerator "Ctrl+V"\
-command "CutPast paste"
$m add command -label [::msgcat::mc "Cut"] -font $font(normal) -accelerator "Ctrl+X"\
-command "CutPast cut"
$m add separator
$m add command -label [::msgcat::mc "Preview formula"] -command {GenerateFormula math} -font $font(normal)\
-accelerator "Alt+F"
$m add command -label [::msgcat::mc "Preview text"] -command {GenerateFormula text} -font $font(normal)\
-accelerator "Alt+T"
$m add separator
$m add command -label [::msgcat::mc "Remove to file"] -command {SaveToFile remove} -font $font(normal)
$m add command -label [::msgcat::mc "Save as file"] -command {SaveToFile file} -font $font(normal)
$m add command -label [::msgcat::mc "Save as image"] -command {SaveToFile image} -font $font(normal)


## VIEW MENU ##
menubutton .frmMenu.mnuView -text [::msgcat::mc "View"] -menu .frmMenu.mnuView.m -font $font(normal) -state normal -background $color(bg)
set m [menu .frmMenu.mnuView.m -background $color(bg)]
$m add cascade -label [::msgcat::mc "Toolbar"] -menu $m.tool -font $font(normal)
set mn [menu $m.tool] 

#$mn add command -label [::msgcat::mc "Create from template"] -command {FileDialog new_templ}\
#-font $font(normal)

$m add separator
$m add command -label [::msgcat::mc "Split window"] -font $font(normal) -accelerator "F2" -state disable\
-command {SplitWindow}
$m add command -label [::msgcat::mc "Encoding"] -font $font(normal) -state normal\
-command {EncodeDialog view}

## MODULES MENU ##
menubutton  .frmMenu.mnuModule  -text [::msgcat::mc "Modules"] -menu .frmMenu.mnuModule.m \
-underline 0 -font $font(normal) -background $color(bg)

## HELP MENU ##
menubutton  .frmMenu.mnuHelp  -text [::msgcat::mc "Help"] -menu .frmMenu.mnuHelp.m \
-underline 0 -font $font(normal) -background $color(bg)
set m [menu .frmMenu.mnuHelp.m -background $color(bg)]
$m  add  command  -label [::msgcat::mc "Help"]  -command  ShowHelp \
-accelerator F1 -font $font(normal)
$m add command -label [::msgcat::mc "About ..."] -command AboutDialog \
-font $font(normal)

pack .frmMenu.mnuFile .frmMenu.mnuEdit .frmMenu.mnuDoc .frmMenu.mnuView .frmMenu.mnuModule -side left
pack .frmMenu.mnuHelp -side right


###############################################
frame .frmBody.frmList -background $color(bg)
frame .frmBody.frmEditor -background $color(bg)
pack .frmBody.frmList .frmBody.frmEditor -side left -fill both

## SPLIT BAR ##
pane::create .frmBody.frmList .frmBody.frmEditor

## TREE FRAME ##

set nbTree [NoteBook .frmBody.frmList.nBook -font $font(normal) -side $gui(tabsLabel) -background $color(bg)]
pack $nbTree -fill both -expand true -padx 2 -pady 2

set f [$nbTree insert end fileTree -text [::msgcat::mc "Files"] -background $color(bg)]
set fTree [ScrolledWindow $f.f -background $color(bg)]
pack $fTree -side top -fill both -expand yes  -padx 0 -pady 0
set tree [Tree $fTree.tree \
-relief sunken -borderwidth 1 -width 1 -height 1 -highlightthickness 1\
-redraw 0 -dropenabled 1 -dragenabled 1 -dragevent 3 \
-background $color(editBg) -selectbackground $color(selectbg) \
-droptypes {TREE_NODE    {copy {} move {} link {}}
LISTBOX_ITEM {copy {} move {} link {}}} -opencmd {} -closecmd  {}]
$fTree setwidget $tree
#pack $tree -side left -fill both -expand yes
###############################################
set fStruct [$nbTree insert end fileStruct -text [::msgcat::mc "Document"] -background $color(bg)]
set fTreeStruct [ScrolledWindow $fStruct.fs -background $color(bg)]
pack $fTreeStruct -side top -fill both -expand yes
set treeStruct [Tree $fTreeStruct.treeStruct \
-relief sunken -borderwidth 1 -width 1 -height 1 -highlightthickness 1\
-redraw 0 -dropenabled 0 -dragenabled 0 -dragevent 3 \
-background $color(editBg) -selectbackground $color(selectbg) \
-showlines 1 -droptypes {
    TREE_NODE    {copy {} move {} link {}}
    LISTBOX_ITEM {copy {} move {} link {}}
}]
$fTreeStruct setwidget $treeStruct
#pack $treeStruct -side left -fill both -expand yes
###############################################
set f [$nbTree insert end lblList -text [::msgcat::mc "Labels"] -background $color(bg)]
set fList [ScrolledWindow $f.f -background $color(bg)]
set lblList [ListBox $fList.list -font $font(normal) -relief sunken -borderwidth 1 -width 1 -height 1 -highlightthickness 1 -background $color(editBg) -selectbackground $color(selectbg)]
#pack $lblList -side left -fill both -expand yes
$fList setwidget $lblList
pack $fList -side top -fill both -expand yes
###############################################
set nb [NoteBook .frmBody.frmEditor.noteBook -font $font(normal) -side $gui(tabsLabel) -background $color(bg)]
pack $nb -fill both -expand true -padx 2 -pady 2
###############################################

set m .popupTabs
menu $m -font $font(normal)
$m add command -label [::msgcat::mc "Close"] -command {FileDialog close}\
-font $font(normal) -accelerator "Ctrl+W"
$m add command -label [::msgcat::mc "Close all"] -command {FileDialog close_all}\
-font $font(normal)

## KEYS BINDINGS ##

$tree bindText <Button-1> "TreeOneClick [$tree selection get]"
$tree bindImage <Button-1> "TreeOneClick [$tree selection get]"
$tree bindText <Shift-Button-1> {$tree selection add [$tree selection get]}
$tree bindImage <Shift-Button-1> {$tree selection add [$tree selection get]}
$treeStruct bindText <Button-1> "TreeStructOneClick [$treeStruct selection get]"
$treeStruct bindImage <Button-1> "TreeStructOneClick [$treeStruct selection get]"

$nb bindtabs <ButtonRelease> "ClickTab"
$nb bindtabs <Button-3> {catch [PopupMenuTab .popupTabs %X %Y]}

## MOUSE WHEEL BINDING ##
bind $fTree.tree.c <Button-4> "$tree yview scroll -3 units"
bind $fTree.tree.c <Button-5> "$tree yview scroll  3 units"
bind $fTree.tree.c <Shift-Button-4> "$tree xview scroll -2 units"
bind $fTree.tree.c <Shift-Button-5> "$tree xview scroll  2 units"
bind $fTree.tree.c <Button-3> {catch [tk_popup .frmMenu.mnuFile.m %X %Y]}

bind $fTreeStruct.treeStruct.c <Button-4> "$treeStruct yview scroll -3 units"
bind $fTreeStruct.treeStruct.c <Button-5> "$treeStruct yview scroll  3 units"
bind $fTreeStruct.treeStruct.c <Shift-Button-4> "$treeStruct xview scroll -2 units"
bind $fTreeStruct.treeStruct.c <Shift-Button-5> "$treeStruct xview scroll  2 units"

$lblList bindImage <ButtonRelease-1> "FindLabel [$lblList selection get]"
$lblList bindText <ButtonRelease-1> "FindLabel [$lblList selection get]"

bind $lblList.c <Button-4> "$lblList yview scroll -3 units"
bind $lblList.c <Button-5> "$lblList yview scroll  3 units"
bind $lblList.c <Shift-Button-4> "$lblList xview scroll -2 units"
bind $lblList.c <Shift-Button-5> "$lblList xview scroll  2 units"

bind . <Control-q> Quit
bind . <Control-Q> Quit
bind . <F1> ShowHelp
bind . <F5> {ConvertDialog dvi}
bind . <F6> {ConvertDialog ps}
bind . <F7> {ConvertDialog pdf}

bind . <Control-Next> {PageTab 1}
bind . <Control-Prior> {PageTab -1}

###############################################
##               CREATE TOOLBARs             ##
###############################################
set btnNum 0
proc CreateToolButton {frame command image hint keys} {
    global dir btnNum color relief
    if {$frame == "main"} {
        set frm frmTool
    } else {
        set frm frmToolEdit
    }
    set comm [string trim $command "\""]
    set btn [Button .$frm.$frame.btn_$btnNum -relief $relief(btn) -bd 1\
    -command $comm -background $color(bg)]
    image create photo img_$btnNum -format gif -file [file join $dir(img) $image]
    $btn configure -image img_$btnNum
    if {$keys != ""} {
        set keys [string trim $keys "\""]
        #puts "$keys - $comm"
        bind . <$keys> "$comm"
    }
        
    pack $btn -side left
    balloon $btn set [::msgcat::mc [string trim $hint "\""]]
    incr btnNum
}
proc ReadToolBarFile {_dir} {
    global dir font  color file frm frame tool
    foreach file [lsort [glob -nocomplain [file join $_dir toolbar *]]] {
        puts "Found toolbar file - \'$file\'"
        set frame [file tail $file]
        if {$frame == "main"} {
            set frm frmTool
        } else {
            set frm frmToolEdit
        }
        AddCheckBox .frmMenu.mnuView.m.tool $frame $file $frm
        if {$tool($frame) == 1} {
            ReadToolBar $file
        }
    }    
}

proc AddCheckBox {m frame file frm} {
    global font color tool
    $m add checkbutton -label [::msgcat::mc "$frame"] -font $font(normal) -state normal\
    -offvalue 0 -onvalue 1 -variable tool($frame)  -selectcolor $color(selectbg)\
    -command "ToolBar $frm $frame $file"\
    -background $color(bg)    
}

proc ReadToolBar {file} {
    global dir font color
    set frame [file tail $file]
    puts "Create toolbar \'$frame\' from file \'$file\'"
    
    if {$frame == "main"} {
        set frm frmTool
    } else {
        set frm frmToolEdit
    }
    pack [frame .$frm.$frame -background $color(bg)] -side left -fill x
    set fHandle [open $file]
    while {[gets $fHandle line]>=0} {
        set line [string trim $line]
        set kw [string range $line 0 [expr [string first " " $line]-1]]
        set l [string range $line [expr [string first " " $line] +1] end]
        if {$kw == "button"} {
            if [regexp -- {(\".*?\") (.*?) (\".*?\") (\".*?\")} $l m cmd img hint keys] {
                #puts "$frame $cmd $img $hint"
                CreateToolButton $frame $cmd $img $hint $keys
            } elseif [regexp -- {(\".*?\") (.*?) (\".*?\")} $l m cmd img hint] {
                CreateToolButton $frame $cmd $img $hint ""
            }
        }
        if {$l == "Separator"} {
            CreateSeparator $frame
        }
        if {$kw == "menu"} {
            if [regexp -- {(.*?) (\".*?\")} $l m img hint] {
                set menu [CreateToolMenu $frame $img $hint]
            }
        }
        if {$kw == "cmd"} {
            
            if [regexp -- {(\".*?\") (\".*?\") (\".*?\")} $l m cmd hint keys] {
                InsertCommand $menu $cmd $hint "" $keys
            } elseif [regexp -- {(\".*?\") (\".*?\")} $l m cmd hint] {
                InsertCommand $menu $cmd $hint "" ""
            }
            
            if [regexp -- {separator} $l m] {
                InsertSeparator $menu
            }
        }
        if {$kw == "imagecmd"} {
            if [regexp -- {(\".*?\") (\".*?\") (.*?)\.gif} $l m cmd hint im] {
                InsertCommand $menu $cmd $hint $im ""
            }
            if [regexp -- {(\".*?\") (\".*?\") (.*?)\.gif (\".*?\")} $l m cmd hint im keys] {
                InsertCommand $menu $cmd $hint $im $keys
            }
        }            
    }
    close $fHandle
}

set mnuNum 0
proc CreateToolMenu {frame image hint} {
    global dir font mnuNum color relief
    if {$frame == "main"} {
        set frm frmTool
    } else {
        set frm frmToolEdit
    }
    image create photo img_mnu$mnuNum -format gif -file [file join $dir(img) $image]
    menubutton .$frm.$frame.m$mnuNum -image img_mnu$mnuNum -border 0\
    -menu .$frm.$frame.m$mnuNum.m -background $color(bg) -relief $relief(menu)
    
    set menu [menu .$frm.$frame.m$mnuNum.m -background $color(bg)]
    pack .$frm.$frame.m$mnuNum -side left
    balloon .$frm.$frame.m$mnuNum set [::msgcat::mc [string trim $hint "\""]]
    incr mnuNum
    return $menu
}
set cmdNum 0
proc InsertCommand {m cmd hint image keys} {
    global font dir cmdNum color
    set comm [string trim $cmd "\""]
    set hint [string trim $hint "\""]
    #puts "Create cmd - $m -  $cmd $hint $image"
    
    if {$keys != ""} {
        set keys [string trim $keys "\""]
        #puts "$keys - $comm"
        bind . <$keys> "$comm"
        if {$image != ""} {
            set image [string trim $image]
            image create photo img_$cmdNum -format gif -file [file join $dir(img) $image.gif]
            $m add command -label [::msgcat::mc $hint] -font $font(normal) -state normal\
            -command $comm -image img_$cmdNum -background $color(bg) -accelerator "$keys"
        } else {
            $m add command -label [::msgcat::mc $hint] -font $font(normal) -state normal\
            -command $comm -background $color(bg) -accelerator "$keys"
        }
    } else {
        if {$image != ""} {
            set image [string trim $image]
            image create photo img_$cmdNum -format gif -file [file join $dir(img) $image.gif]
            $m add command -label [::msgcat::mc $hint] -font $font(normal) -state normal\
            -command $comm -image img_$cmdNum -background $color(bg)
        } else {
            $m add command -label [::msgcat::mc $hint] -font $font(normal) -state normal\
            -command $comm -background $color(bg)
        }
    }
    incr cmdNum
}
proc InsertSeparator {m} {
    $m add separator
}

proc CreateSeparator {frame} {
    global dir btnNum color relief
    if {$frame == "main"} {
        set frm frmTool
    } else {
        set frm frmToolEdit
    }
    set btn [frame .$frm.$frame.btn_$btnNum -width 10 -background $color(bg) -borderwidth 0]
    #set btn [Button .$frm.$frame.btn_$btnNum -relief $relief(btn) -bd 2 -background $color(bg) -width 0 -state disable]
    pack $btn -side left -fill y -expand 1
    incr btnNum
}

proc ToolBar {frm frame file} {
    global tool
    if {$tool($frame) == 0} {
        #set tool($frame) 0
        if {$frame == "main"} {
            destroy .$frm.$frame
            .frmTool configure -height 1
        } else {
            destroy .$frm.$frame
            .frmToolEdit configure -height 1
        }
        return
    } elseif {$tool($frame) == 1} {
        #set tool($frame) 1
        ReadToolBar $file
        return
    }
}

###############################################
proc CutPast {action} {
    global nb
    set node [$nb raise]
    if {$node == "newproj" || $node == "settings" || $node == "about" || $node == ""} {
        return
    }
    
    set text $nb.f$node.f.text
    switch -- $action {
        "copy" {
            tk_textCopy $text
        }
        "cut" {
            tk_textCut $text
        }
        "paste" {
            tk_textPaste $text
        }
        "undo" {
            $text undo
        }
    }
}
proc Select {type} {
    global nb activeFile
    set text $nb.f$activeFile.f.text
    if {$type == "all"} {
        $text tag remove sel 1.0 end
        $text tag add sel 1.0 end
    } elseif {$type == "remove"} {
        $text tag remove sel 1.0 end
    }
}

proc PopupMenu {text x y} {
    set selIndex [$text tag ranges sel]
    if {$selIndex != ""} {
        tk_popup .frmMenu.mnuEdit.m.selection $x $y
    } else {
        tk_popup .frmMenu.mnuEdit.m $x $y
    }
}
proc PopupMenuTab {menu x y} {
    tk_popup $menu $x $y
}

###########
## STATUS BAR ##
set frm1 [frame .frmStatus.frmFileAttr -background $color(bg)]
set frm2 [frame .frmStatus.frmActive -background $color(bg)]
set frm3 [frame .frmStatus.frmFileSize -relief sunken -background $color(bg)]
set frm4 [frame .frmStatus.frmPos -background $color(bg)]
set frm5 [frame .frmStatus.frmFile -background $color(bg)]
set frm6 [frame .frmStatus.frmOvwrt -background $color(bg)]
set frm7 [frame .frmStatus.frmOther -background $color(bg)]
pack $frm1 $frm4 $frm6 $frm2 $frm5 $frm3 -side left -fill x
pack $frm7 -side left -fill x -expand true
set status(fileAttr) [label $frm1.lblFileAttr -width 30 -relief sunken -font $font(normal) -anchor w -background $color(bg)]
pack $frm1.lblFileAttr -fill x
set status(pos) [label $frm4.lblPos -width 10 -relief sunken -font $font(bold) -anchor w -background $color(bg)]
pack $frm4.lblPos -fill x
set status(ovwrt) [label $frm6.lblOvwrt -width 10 -relief sunken -font $font(normal) -anchor center -background $color(bg)]
pack $frm6.lblOvwrt -fill x
set status(active) [label $frm2.lblActive -width 25 -relief sunken -font $font(normal) -anchor center -background $color(bg) ]
pack $frm2.lblActive -fill x
set status(fileSize) [label $frm3.lblFileSize -width 10 -relief sunken -font $font(normal) -anchor w -background $color(bg)]
pack $frm3.lblFileSize -fill x
set status(encode) [label $frm5.lblFile -width 10 -relief sunken -font $font(normal) -anchor w -background $color(bg)]
pack $frm5.lblFile -fill x
set status(other) [label $frm7.lblOther -width 10 -relief sunken -font $font(normal) -anchor w -background $color(bg)]
pack $frm7.lblOther -fill x

#######
bind . <Control-n> {FileDialog new}
bind . <Control-ograve> {FileDialog new}
bind . <Control-o> {FileDialog open}
bind . <Control-ugrave> {FileDialog open}

$nbTree raise fileTree
############
$status(ovwrt) configure -text [::msgcat::mc "Insert"]

ReadToolBarFile $dir(gui) ;# system toolbars
ReadToolBarFile $dir(work_gui) ;# user toolbars
ReadHotKeysFile $dir(gui) ;# system hotkeys
ReadHotKeysFile $dir(work_gui) ;# users hotkeys

## FINDE EXTERNAL MODULES ##
FindModule





