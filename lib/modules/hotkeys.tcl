######################################################
#                Tk LaTeX Editor
#        Distributed under GNU Public License
# Author: Sergey Kalinin (aka BanZaj) banzaj@lrn.ru
# Copyright (c) "CONERO lab", 2004, http://conero.lrn.ru
######################################################

proc ReadHotKeysFile {_dir} {
    global dir
    foreach file [lsort [glob -nocomplain [file join $_dir hotkeys *]]] {
        puts "Found hotkeys file - \'$file\'"
        ReadHotKeys $file
    }    
}


set mnum 0
proc ReadHotKeys {file} {
    global dir font mnum
    set menu_root ".frmMenu.mnuEdit.m.hotkeys"
    set menu $menu_root
    puts "Read HotKeys settings from \'$file\'"
    
    set fHandle [open $file]
    while {[gets $fHandle line]>=0} {
        set line [string trim $line]
        set kw [string range $line 0 [expr [string first " " $line]-1]]
        set l [string range $line [expr [string first " " $line] +1] end]
        if {$l == "Separator"} {
            CreateSeparator $frame
        }
        if {$kw == "menu"} {
            if [regexp -- {(.*?) (\".*?\")} $l m img hint] {
                $menu_root add cascade -label $hint -menu $menu.m_$mnum -font $font(normal)
                set menu [menu $menu.m_$mnum] 
                incr mnum
            }
        }
        if {$kw == "cmd"} {
            if [regexp -- {(\".*?\") (\".*?\") (\".*?\")} $l m cmd hint keys] {
                InsertCommand $menu $cmd $hint "" $keys
            } elseif [regexp -- {(\".*?\") (\".*?\")} $l m cmd hint] {
                InsertCommand $menu $cmd $hint "" ""
            }
            if [regexp -- {separator} $l m] {
                InsertSeparator $menu
            }
        }
        if {$kw == "imagecmd"} {
            if [regexp -- {(\".*?\") (\".*?\") (.*?)\.gif} $l m cmd hint im] {
                InsertCommand $menu $cmd $hint $im ""
            }
            if [regexp -- {(\".*?\") (\".*?\") (.*?)\.gif (\".*?\")} $l m cmd hint im keys] {
                InsertCommand $menu $cmd $hint $im $keys
            }
        }
    }
    close $fHandle
    
}

