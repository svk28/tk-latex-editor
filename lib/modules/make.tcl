#########################################################
#                TkTeXEditor
#        Distributed under GNU Public License
# Author: Sergey Kalinin (BanZaj) banzaj@lrn.ru
# Copyright (c) "CONERO lab", 2002, http://conero.lrn.ru
#########################################################

## INSERT DEBUG INFORMATION INTO TEXT WIDGET ##
proc DebugInfo {widget file folder} {
    global font nb w errLine activeFile
    set w $widget
    $widget configure -state normal
    if {[winfo depth $nb] > 1} {
        set bold "-background blue -foreground white -relief raised -borderwidth 1"
        set normal "-background {} -foreground blue -relief flat"
    } else {
        set bold "-foreground white -background black"
        set normal "-foreground {} -background {}"
    }

    $widget tag configure error -font $font(bold) -foreground red
    $widget tag configure erName -font $font(bold) -foreground blue
    $widget tag bind erName <Any-Enter> "$widget tag configure erName $bold; $widget configure -cursor hand2"
    $widget tag bind erName <Any-Leave> "$widget tag configure erName $normal; $widget configure -cursor xterm"
    
    if {[eof $file]} {
        catch {close $file} msg
        if {$msg != ""} {
            #            puts $msg
            $widget insert end "[::msgcat::mc "Program failed"]: $msg\n";
        } else {
            #            puts $msg
            $widget insert end "\n-----------------------------------------------\n"
            $widget insert end "[::msgcat::mc "Program finished successfully"]\n"
            TempFilesDel $folder
        }
    } else {
        set line [read $file]
        set index [string first "!" $line]
        $widget insert end $line
        set pos [$widget index insert]
        set lineNumber [lindex [split $pos "."] 0]
        if {$index != -1} {
            set p [$widget search -backward -- "!" $pos 0.0]
            set l [lindex [split $p "."] 0]
            set c [lindex [split $p "."] 1]
            
            $widget tag add error $l.0 $l.end
            set errLine [expr $l + 1]
            $widget tag add erName $errLine.0 $errLine.end
            $widget tag bind erName <ButtonPress-1> {ErrorFind}
        }
    }
    $widget see end
    $widget configure -state disabled
}

## FIND COMPILE ERROR INTO TEXT BUFFER ##
proc ErrorFind {} {
    global font nb w errLine activeFile
    set word [$w get $errLine.0 $errLine.end]
    set error [string range $word 2 [string first " " $word]]
    set text "$nb.f$activeFile.f.text"
    focus -force $text
    $text mark set insert [string trim $error].0
    $text see insert
}


## DELETE TEMPORARY FILES ##
proc TempFilesDel {folder} {
    foreach ext {log bak tmp} {
        foreach file [lsort [glob -nocomplain [file join $folder *.$ext]]] {
            file delete -- $file
        }
    }
}


