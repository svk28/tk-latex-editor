#########################################################
#                Tk LaTeX Editor
#        Distributed under GNU Public License
# Author: Sergey Kalinin (BanZaj) banzaj@lrn.ru
# Copyright (c) "CONERO lab", 2002, http://conero.lrn.ru
#########################################################

proc FindModule {} {
    global module dir font tcl_platform color preview_cmd convert_cmd
    set menu [menu .frmMenu.mnuModule.m -background $color(bg)]
    set moduleName ""
    
    foreach m {tkcvs tkdiff dvi ps pdf html} {
        if [info exists convert_cmd($m)] {
            scan $convert_cmd($m) "%s" moduleName
            FindExtMod_ convert_$m $moduleName
        }
        if [info exists preview_cmd($m)] {
            scan $preview_cmd($m) "%s" moduleName
            FindExtMod_ preview_$m $moduleName
        } else {
            FindExtMod $m $m
        }
        
    }
}
## Find external programms ##
proc FindExtMod {m cmd} {
    global module dir font tcl_platform color preview_cmd convert_cmd
    if {$tcl_platform(platform) == "unix"} {
        set string [exec whereis $cmd]
        scan $string "%s%s" v module($m)
        if {[info exists module($m)] &&  [file isdirectory $module($m)] == 0} {
            puts "Find external programm $module($m)"
            .frmMenu.mnuModule.m add command -label [::msgcat::mc "$m"] -command "DoModule $m" \
            -font $font(normal)
        } else {
            set module($m) ""
        }
    } elseif {$tcl_platform(platform) == "mac"} {
        set module($m) $cmd
    } elseif {$tcl_platform(platform) == "windows"} {
        set module($m) $cmd
    }
}
proc FindExtMod_ {m cmd} {
    global module dir font tcl_platform color preview_cmd convert_cmd
    if {$tcl_platform(platform) == "unix"} {
        set string [exec whereis $cmd]
        scan $string "%s%s" v module($m)
        if {[info exists module($m)] &&  [file isdirectory $module($m)] == 0} {
            puts "Find external programm $module($m)"
        } else {
            set module($m) ""
        }
    } elseif {$tcl_platform(platform) == "mac"} {
        set module($m) $cmd
    } elseif {$tcl_platform(platform) == "windows"} {
        set module($m) $cmd
    }
}

## RUNNING MODULE ##
proc DoModule {mod} {
    global tcl_platform
    global module dir tree
    case $mod {
        tkcvs {
            set pipe [open "|$module(tkcvs) -dir $dir(doc)" "r"]
            fileevent $pipe readable
            fconfigure $pipe -buffering none -blocking no
        }
        tkdiff {
            set selFiles [$tree selection get]
            if {[llength $selFiles] == 0} {
                set answer [tk_messageBox\
                -message "[::msgcat::mc "Don't selected file"]"\
                -type ok -icon warning\
                -title [::msgcat::mc "Warning"]]
                case $answer {
                    ok {return 0}
                }
            }
            if {[llength $selFiles] == 1} {
                if {$selFiles != ""} {
                    set file1 [$tree itemcget $selFiles -data]
                }
                set command "-r $file1"
            }
            if {[llength $selFiles] == 2} {
                if {[lindex $selFiles 0] != ""} {
                    set file1 [$tree itemcget [lindex $selFiles 0] -data]
                }
                if {[lindex $selFiles 1] != ""} {
                    set file2 [$tree itemcget [lindex $selFiles 1] -data]
                }
                set command "$file1 $file2"
            } 
            if {[llength $selFiles] > 2} {
                set answer [tk_messageBox\
                -message "[::msgcat::mc "Must be one or two file select!"]"\
                -type ok -icon info\
                -title [::msgcat::mc "Warning"]]
                case $answer {
                    ok {return 0}
                }
            }
            set pipe [open "|$module(tkdiff) $command" "r"]
            fileevent $pipe readable
            fconfigure $pipe -buffering none -blocking no
        }
    }
}


proc EndProc {module file} {
    if {[eof $file]} {
        catch [close $file] msg
        if {$msg != ""} {
            puts $msg
            Message "$module\n [::msgcat::mc "Program failed"]: $msg\n";
        } else {
            Message "$module\n [::msgcat::mc "Program finished successfully"]\n"
        }
    } else {
        read $file
    }
}

proc Message {message} {
    global font
    set answer [MessageDlg .msg -parent .\
    -message "$message"\
    -type ok -icon warning\
    -font $font(normal)\
    -title [::msgcat::mc "Warning"]]
    case $answer {
        ok {return 0}
    }
}




