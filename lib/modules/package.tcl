######################################################
#                TkTeXEditor
#        Distributed under GNU Public License
# Author: Sergey Kalinin (BanZaj) banzaj@lrn.ru
# Copyright (c) "CONERO lab", 2002, http://conero.lrn.ru
######################################################

proc GetPackage {_dir} {
    global nb dir cmd
    foreach file [lsort [glob -nocomplain [file join $_dir *.lst]]] {
        puts "Load commands from $file"
        set pkgName [file rootname [file tail $file]]
        set cmd($pkgName) ""
        #set cmd(all) ""
        set f [open $file r]
        while {[gets $f line] >= 0} {
            if {[string range $line 0 0] != "#"} {
                if [info exists cmd($pkgName)] {
                    lappend cmd($pkgName) $line
                    lappend cmd(all) $line
                } else {
                    set cmd($pkgName) [list $line]
                    set cmd(all) [list $line]
                }
            }
        }
        close $f
    }
    puts "����� ������ - [llength $cmd(all)]" ;# debug info
}

proc PackageDialog {} {
    global nb font dir color
    set w .frmBody.packages
    # destroy the find window if it already exists
    if {[winfo exists $w]} {
        destroy $w
    }
    
    toplevel $w
    wm title $w [::msgcat::mc "Pakages"]
    wm resizable $w 0 0
    #wm transient .
    
    label $w.text -text [::msgcat::mc "Included packages"] -font $font(bold)\
    -relief groove -border 1
    frame $w.frmPkg -relief groove -border 1  -background $color(bg)
    frame $w.frmBtn -background $color(bg)
    pack $w.text -side top -anchor nw -fill x
    pack $w.frmPkg         -side top -fill both -expand 1
    pack $w.frmBtn -side top -fill x    
    
    foreach pkg [GetPackage] {
        checkbutton $w.frmPkg.chk_$pkg -text $pkg  -font $font(normal) -background $color(bg)
        pack $w.frmPkg.chk_$pkg -side top -anchor w -padx 2 
    }
    
    button $w.frmBtn.btnOk -text [::msgcat::mc "Save"] -relief groove -font $font(normal)\
    -command {    } -background $color(bg)
    button $w.frmBtn.btnCancel -text [::msgcat::mc "Close"] -relief groove -font $font(normal)\
    -command "destroy $w" -background $color(bg)
    pack $w.frmBtn.btnOk $w.frmBtn.btnCancel -padx 2 -pady 2 -fill x -side left
    
}








