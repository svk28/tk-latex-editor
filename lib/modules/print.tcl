######################################################
#                TkTeXEditor
#        Distributed under GNU Public License
# Author: Sergey Kalinin (BanZaj) banzaj@lrn.ru
# Copyright (c) "CONERO lab", 2002, http://conero.lrn.ru
######################################################

proc PrintDialog {action} {
    global dir font nb files activeFile tcl_platform
    
    if [info exists activeFile] {
        set node $activeFile
    } else {
        return
    }
    if {$node == "newproj" || $node == "settings" || $node == "about" || $node == ""} {
        return
    }
    if {[info exists files($node)] == 0} {
        return
    }
    switch -- $action {
        "print" {
            PrintForm
        }
        "set" {
        }
        "preview" {
            set text "$nb.f$node.f.text"
            set fullPath [lindex $files($node) 0]
            set folder [file dirname $fullPath]
            set file [file tail $fullPath]
            set ext [string trim [file extension $file] {.}]
            set name [file rootname $file]
            set file [file join $folder $name.ps]
            
            if {$tcl_platform(platform) == "unix"} {
                set pipe [open "|gv $file" "r"]
            } elseif {$tcl_platform(platform) == "mac"} {
                
            } elseif {$tcl_platform(platform) == "windows"} {
                set pipe [open "|gsview32 $file" "r"]
            }
            
            fileevent $pipe readable
            fconfigure $pipe -buffering none -blocking no
        }
    }
}


proc PrintForm {} {
    global font selectPrint color
    set wp .print
    # destroy the print window if it already exists
    if {[winfo exists $wp]} {
        destroy $wp
    }
    
    toplevel $wp
    wm transient $wp .
    wm title $wp [::msgcat::mc "Print ..."]
    wm resizable $wp 0 0
    frame $wp.frmLbl -background $color(bg)
    frame $wp.frmEnt -background $color(bg)
    frame $wp.frmField -background $color(bg)
    frame $wp.frmBtn -background $color(bg)
    pack $wp.frmLbl $wp.frmEnt $wp.frmField $wp.frmBtn -side top -fill x
    label $wp.frmLbl.lblPrint -text [::msgcat::mc "Print command"] -font $font(normal) -background $color(bg)
    pack $wp.frmLbl.lblPrint -fill x -expand true -padx 2
    entry $wp.frmEnt.entPrint -font $font(normal)
    pack $wp.frmEnt.entPrint -fill x -expand true -padx 2
    
    checkbutton $wp.frmField.chkSelect -text [::msgcat::mc "Print selected text"] -variable selectPrint\
    -font $font(normal) -onvalue true -offvalue false\
    -background $color(bg) -selectcolor $color(selectbg);#-command Check
    pack $wp.frmField.chkSelect -fill x -expand true -padx 2 -anchor w
    
    button $wp.frmBtn.btnPrint -text [::msgcat::mc "Print"] -font $font(normal) -width 12\
    -relief groove -background $color(bg)\
    -command {
        Print [.print.frmEnt.entPrint get]
        destroy .print
    }
    button $wp.frmBtn.btnCancel -text [::msgcat::mc "Cancel"] -font $font(normal) -width 12 -relief groove\
    -command "destroy .print" -background $color(bg)
    pack $wp.frmBtn.btnPrint $wp.frmBtn.btnCancel -side left -padx 2 -pady 2 -fill x -expand true
    $wp.frmEnt.entPrint insert 0 "lpr"
    bind $wp <Escape> "destroy .print"
}

proc Preview {action} {
    global dir font nb files activeFile tcl_platform preview_cmd module tree
    if [info exists activeFile] {
        set node $activeFile
        while {[set parentNode [$tree parent $node]] != "root"} {
            set node $parentNode
        }        
    } else {
        return
    }
    if {$node == "newproj" || $node == "settings" || $node == "about" || $node == ""} {
        return
    }
    if {[info exists files($node)] == 0} {
        return
    }
    if {$module(preview_$action) == ""} {
        set answer [tk_messageBox\
        -message "[::msgcat::mc "Don't find programm"] $preview_cmd($action)"\
        -type ok -icon warning\
        -title [::msgcat::mc "Warning"]]
        case $answer {
            ok {return 0}
        }
    }
    set text "$nb.f$node.f.text"
    set fullPath [lindex $files($node) 0]
    set file [file tail $fullPath]
    set ext [string trim [file extension $file] {.}]
    set name [file rootname $file]
    if {$action == "html"} {
        set folder [file join [file dirname $fullPath] $name]
    } else {
        set folder [file dirname $fullPath]
    }
    
    set pipe [open "|$preview_cmd($action) [file join $folder $name.$action]" "r"]
    puts "$preview_cmd($action) [file join $folder $name.$action]"
    fileevent $pipe readable
    fconfigure $pipe -buffering none -blocking no
}






