######################################################
#                TkTeXEditor
#        Distributed under GNU Public License
# Author: Sergey Kalinin (BanZaj) banzaj@lrn.ru
# Copyright (c) "CONERO lab", 2002, http://conero.lrn.ru
######################################################

######################################################
#                                                    #
#        Notebook tabs procedures and other          #
#                                                    #
######################################################


## CLICKING TUBULAR HEADER PROCEDURE ##
proc ClickTab {node} {
    global nb files activeFile tree ver files status release
    set node [$nb raise]
    if {($node == "") || ($node == "debug")} {return}
    set activeFile $node
    set text "$nb.f$node.f.text"
    set fullPath [lindex $files($node) 0]
    wm title . "TkLaTeXEditor $ver$release - $fullPath"
    set folder [file dirname $fullPath]
    set file [file tail $fullPath]
    set ext [string trim [file extension $file] {.}]
    set name [file rootname $file]
    $tree selection set $node
    $tree see $node
    UpdateStruct $fullPath $node
    focus -force $text
    unset text fullPath folder file ext name
    ## status bar information update ##
    $status(fileAttr) configure -text "[FileAttr [lindex $files($node) 0]]"
    $status(fileSize) configure -text "[file size [lindex $files($node) 0]] b."
    $status(encode) configure -text "[lindex $files($node) 2]"
    $status(pos) configure -text [$nb.f$node.f.text index insert];# cursor position
}

## NOTEBOOK PAGE SWITCHER ##
proc PageTab {key} {
    global nb
    set len [llength [$nb pages]]
    if {$len > 0} {
        set newIndex [expr [$nb index [$nb raise]] + $key]
        if {$newIndex < 0} {
            set newIndex [expr $len - 1]
        } elseif {$newIndex >= $len} {
            set newIndex 0
        }
        $nb see [lindex [$nb pages] $newIndex]
        $nb raise [lindex [$nb pages] $newIndex]
        PageRaise [lindex [$nb pages] $newIndex]
    }
}
## RAISED NOTEBOOK TAB IF CLICK MOUSE BUTTON ##
proc PageRaise {node} {
    global nb tree files status activeFile ver release
    set parentNode [$tree parent $node]
    if {$node == "debug"} {return}
    if {[$tree exists $node] ==1 && [info exists files($node)] != 1} {
        EditFile [$tree itemcget $node -data] ;# open editor if file don't open
    } elseif {[$tree exists $node] ==0} {
        set file [file tail [lindex $files($node) 0]]
        set fileDir [file dirname [lindex $files($node) 0]]
        set node [InsertTreeSubNode "$file" $parentNode $fileDir]
    }
    $nb raise $node
    
    set activeFile $node
    #puts "node - $node\nparent - $parentNode"
    $tree selection set $node
    $tree see $node
    set item [$tree itemcget $node -data]
    focus -force $nb.f$node.f.text
    catch [$status(pos) configure -text [$nb.f$node.f.text index insert]] ;# cursor position
    ## status bar information update ##
    $status(fileAttr) configure -text "[FileAttr $item]"
    $status(fileSize) configure -text "[file size $item] b."
    $status(encode) configure -text "[lindex $files($node) 2]"
    $status(pos) configure -text [$nb.f$node.f.text index insert];# cursor position
    UpdateStruct [lindex $files($node) 0] $node;# exec structure updating
    wm title . "TkLaTeXEditor $ver$release - [lindex $files($node) 0]"
    unset item
}


## CLOSE PROGRAMM PROCEDURE ##
proc Quit {} {
    if {[FileDialog quit_close_all] == 0} {
        return
    } else {
        exit
    }    
}

## AUTOSAVE AND AUTOREFRESH TIMERS PROCEDURE ##
set activeTimer(refresh) ""
set activeTimer(autosave) ""
proc Timer {file type} {
    global timers activeFile activeTimer
    if [info exists activeFile] {
    } else {
        return
    }
    if {$type == "refresh"} {
        after cancel $activeTimer(refresh) ;# close previouse task
        set activeTimer(refresh) [after $timers(refresh) {UpdateStruct $file $activeFile}]
    } elseif {$type == "autosave"} {
        after cancel $activeTimer(autosave) ;# close previouse task
        set activeTimer(autosave) [after $timers(autosave) {FileDialog save_all}]
    }
}


proc GetExtention {file} {
    global fileList
    set ext [string range [file extension [file tail $file]] 1 end]
    return $ext
}




