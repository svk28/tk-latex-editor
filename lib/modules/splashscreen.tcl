######################################################
#                Tk LaTeX Editor
#        Distributed under GNU Public License
# Author: Sergey Kalinin (aka BanZaj) banzaj@lrn.ru
# Copyright (c) "CONERO lab", 2004, http://conero.lrn.ru
######################################################

proc ShowSplashScreen {} {
    global font
    global dir tree nb ver font release
    set max 200
    set w .about
    if {[winfo exists $w]} {
        destroy $w
    }
    toplevel $w
    wm title $w [::msgcat::mc "Strting Tk LaTeX Editor ..."]
    #wm resizable $w 0 0
    wm transient $w .
    
    frame $w.frmImg -borderwidth 2 -relief ridge -background black
    image create photo imgAbout -format gif -file [file join $dir(img) tle.gif]
    image create photo imgConero -format gif -file [file join $dir(img) conero.gif]
    
    label $w.frmImg.lblImg -image imgAbout -background black
    
    pack $w.frmImg.lblImg -side top -fill x
    
    frame $w.frmlbl -borderwidth 2 -relief ridge -background "#8700d1"
    label $w.frmlbl.lblVersion -text "[::msgcat::mc Version] $ver$release"\
    -background "#8700d1" -foreground black
    label $w.frmlbl.lblAuthorName -text "[::msgcat::mc Authors]:"\
    -background "#8700d1" -foreground  black
    label $w.frmlbl.lblAuthor1 -text "Sergey Kalinin (aka BanZaj), banzaj@lrn.ru"\
    -background "#8700d1" -foreground  black
    label $w.frmlbl.lblAuthor2 -text "Constantin I Makhailenko (aka Const), const@mail.ru"\
    -background "#8700d1" -foreground  black
    
    
    frame $w.frame
    
    
    ProgressBar $w.frame.bar     \
    -variable   splash_count  -width 100  -height  10  -maximum  $max
    label $w.frame.info  -textvariable info
    
    pack $w.frmlbl.lblVersion $w.frmlbl.lblAuthorName $w.frmlbl.lblAuthor1 $w.frmlbl.lblAuthor2 -side top -padx 5
    pack $w.frmImg -side top -fill x
    pack $w.frmlbl  -side top -expand true -fill both
    
    pack $w.frame.bar $w.frame.info -side top -expand true -fill x
    
    bind $w <Escape> "destroy .about"
    bind $w <Return> "destroy .about"
    #
}

#ShowSplashScreen

