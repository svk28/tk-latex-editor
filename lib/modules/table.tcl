######################################################
#                Tk LaTeX Editor
#        Distributed under GNU Public License
# Author: Sergey Kalinin (BanZaj) banzaj@lrn.ru
# Copyright (c) "CONERO lab", 2002, http://conero.lrn.ru
######################################################

proc InsertTableDialog {} {
    global nb font files findString replaceString text dir color
    set node [$nb raise]
    if {$node == "newproj" || $node == "settings" || $node == "about" || $node == ""} {
        return
    }
    #set file $files($node)
    set w .table
    set text "$nb.f$node.f.text"
    #    set findString ""
    # destroy the find window if it already exists
    if {[winfo exists $w]} {
        destroy $w
    }
    set colNum 1
    set posTable "center"
    set align "center"
    set line "full"
    toplevel $w
    wm transient $w $nb.f$node
    wm title $w [::msgcat::mc "Add table"]
    wm resizable $w 0 0
    
    set f1 [frame $w.frmAlign -borderwidth 1 -relief raised -background $color(bg)]
    set f2 [frame $w.frmTable -borderwidth 1 -relief raised -background $color(bg)]
    set f3 [frame $w.frmBtn -borderwidth 1 -background $color(bg)]
    set f4 [frame $w.frmCol -borderwidth 1 -relief raised -background $color(bg)]
    set f5 [frame $w.frmPos -borderwidth 1 -relief raised -background $color(bg)]
    pack $f4 $f1 $f2 $f5 $f3 -side top -fill x -expand true
    
    label $f4.lblCol -text [::msgcat::mc "Input number of columns"] -background $color(bg)
    SpinBox $f4.spinCol -textvariable colNum -range {1 100 1} -helptext "Columns" -background $color(bg)
    pack $f4.lblCol $f4.spinCol -side top -fill x -pady 2 -padx 2
    
    label $f1.lbl -text [::msgcat::mc "Alignment"] -background $color(bg)
    pack $f1.lbl -side top -fill x
    foreach btn {center left right} {
        radiobutton $f1.btn$btn -text [::msgcat::mc "Align $btn"] -variable align\
        -value $btn -font $font(normal) -selectcolor $color(selectbg) -background $color(bg)
        pack $f1.btn$btn -side top -anchor w
    }
    
    label $f5.lblPos -text [::msgcat::mc "Table position"] -background $color(bg)
    pack $f5.lblPos -side top -fill x
    foreach tbl {top bottom center} {
        image create photo img_$tbl -file [file join $dir(img) table_$tbl.gif]
        radiobutton $f5.btn$tbl -variable table -indicatoron 0 -background $color(bg)\
        -font $font(normal) -image img_$tbl -value $tbl  -selectcolor $color(selectbg)
        pack $f5.btn$tbl -side left -anchor n -pady 2
    }
    
    label $f2.lblLine -text [::msgcat::mc "Line type"] -background $color(bg)
    pack $f2.lblLine -side top -fill x
    foreach tbl {empty full double cols lines} {
        image create photo img_$tbl -file [file join $dir(img) table_$tbl.gif]
        radiobutton $f2.btn$tbl -variable line -indicatoron 0 -background $color(bg)\
        -font $font(normal) -image img_$tbl -value $tbl -selectcolor $color(selectbg)
        pack $f2.btn$tbl -side left -anchor n -pady 2
    }
    
    
    button $f3.btnAdd -text "[::msgcat::mc "Insert"] - Enter" -font $font(normal)\
    -relief groove  -background $color(bg) -command {
        InsertTable $colNum $table $line $align
        destroy .table
    }
    button $f3.btnCancel -text "[::msgcat::mc "Cancel"] - Esc" -command "destroy $w"\
    -font $font(normal) -relief groove -background $color(bg)
    pack $f3.btnAdd $f3.btnCancel\
    -side left -padx 2 -pady 2 -fill x
    
    bind $w <Return> ""
    bind $w <F4> ""
    bind $w <Escape> "destroy $w"
}

proc InsertTable {colNum posTable line align} {
    global dir font color nb
    set node [$nb raise]
    if {$node == ""} {return}
    set text "$nb.f$node.f.text"
    set pos [$text index insert]
    set curLine [lindex [split $pos "."] 0]
    set cursor [lindex [split $pos "."] 1]
    set editLine [$text get $curLine.0 $pos]
    set header "\\begin\{tabular\}"
    set footer "\\end\{tabular\}"
    
    if {$line == "double"} {
        set split "||"
        set lineEnd "\\\\ \\hline\n"
    } elseif {$line == "full"} {
        set split "|"
        set lineEnd "\\\\ \\hline\n"
    } elseif {$line == "empty"} {
        set split " "
        set lineEnd "\\\\\n"
    } elseif {$line == "cols"} {
        set split "|"
        set lineEnd "\\\\\n"
    } elseif {$line == "lines"} {
        set split " "
        set lineEnd "\\\\ \\hline\n"
    } else {
        set split " "
        set lineEnd "\\\\\n"
    }
    switch -- $posTable {
        top        {set position "t"}
        bottom        {set position "b"}
        center        {set position "c"}
    }
    switch -- $align {
        center        {set colAlign "c"}
        left        {set colAlign "l"}
        right        {set colAlign "r"}
    }
    set count 0
    set count_ 0
    while {$colNum > $count} {
        append columns "$split$colAlign"
        incr count
    }
    while {$colNum > [expr $count_+1]} {
        append body "&  "
        incr count_
    }
    
    append columns $split
    
    $text insert $pos "$header\[$position\]\{$columns\} $lineEnd\t$body$lineEnd$footer"
    $text mark set insert [expr $curLine +1].1
    $text see insert
    
}





