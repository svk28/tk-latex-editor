######################################################
#                TkTeXEditor
#        Distributed under GNU Public License
# Author: Sergey Kalinin (aka BanZaj) banzaj@lrn.ru
# Copyright (c) "CONERO lrn", 2002, http://conero.lrn.ru
######################################################

proc TemplateDialog {} {
    global dir nb tmpl font color  relief
    set w .template
    if {[winfo exists $w]} {
        destroy $w
    }
    # create the new "goto" window
    toplevel $w
    wm title $w [::msgcat::mc "Create new file"]
    #wm resizable $w 0 0
    wm geometry $w 550x300
    wm transient $w .
    
    frame $w.frmCanv -border 1 -relief sunken
    frame $w.frmBtn -border 1 -relief sunken
    frame $w.frmTxt -border 1 -relief sunken
    pack $w.frmCanv -side top -fill both -padx 1 -pady 1 -expand 1 -fill both
    pack $w.frmTxt -side top -fill x
    pack $w.frmBtn -side top -fill x
    
    set fList [ScrolledWindow $w.frmCanv.f -background $color(bg)]
    set list [ListBox $fList.list -font $font(normal)\
    -relief sunken -borderwidth 1 -width 1 -height 1 -highlightthickness 1\
    -background $color(editBg) -selectbackground $color(selectbg)]
    pack $list -side left -fill both -expand yes
    $fList setwidget $list
    pack $fList -side left -fill both -expand yes
    
    
    set fText [ScrolledWindow $w.frmCanv.f1 -background $color(bg)]
    set text [supertext::text $fText.txtTemplate -width 5 -height 5 -yscrollcommand "$w.frmCanv.yScrText set" \
    -wrap none -state disable -background $color(editBg) -foreground $color(editFg)\
    -selectborderwidth 0 -selectbackground $color(selectbg)]
    $fText setwidget $text
    pack $fText -side left -fill both -expand 1
    #pack $text -side left -fill both -expand 1
    #return
    
    ## SPLIT BAR ##
    pane::create $fList $fText
    
    label $w.frmTxt.lblFile -text [::msgcat::mc "Input file name"]  -font $font(normal)
    entry $w.frmTxt.entFile
    pack $w.frmTxt.lblFile $w.frmTxt.entFile -side left -padx 2
    pack $w.frmTxt.entFile -side left -padx 1 -fill x -expand true
    $w.frmTxt.entFile insert 0 "name.tex"
    
    button $w.frmBtn.btnOk -text [::msgcat::mc "Create"] -relief groove  -font $font(normal) -command {
        set file [.template.frmTxt.entFile get]
        file copy -force $tmpl [file join $dir(current) $file]
        EditFile [file join $dir(current) $file]
        destroy .template
    }
    
    button $w.frmBtn.btnCancel -text [::msgcat::mc "Close"] -command "destroy $w" -relief groove -font $font(normal)
    pack $w.frmBtn.btnOk $w.frmBtn.btnCancel -padx 2 -pady 2 -fill x -side left
    
    bind $w <Escape> "destroy .template"
    focus -force $list
    $list bindImage <ButtonRelease-1> "ReadTemplateDescr [$list selection get]"
    $list bindText <ButtonRelease-1> "ReadTemplateDescr [$list selection get]"
    bind $list.c <Button-4> "$list yview scroll -3 units"
    bind $list.c <Button-5> "$list yview scroll  3 units"
    bind $list.c <Shift-Button-4> "$list xview scroll -2 units"
    bind $list.c <Shift-Button-5> "$list xview scroll  2 units"
    
    GetTemplateList $list
}

proc GetTemplateList {list} {
    global dir
    set ind 0
    foreach file [lsort [glob -nocomplain [file join $dir(tmpl) *.tex]]] {
        if {[file isdirectory $file] == 0} {
            #$list insert end $file
            $list insert end tmpl_$ind -text [file tail $file] \
            -image [Bitmap::get [file join $dir(img) tex_tmpl.gif]] \
            -data $file
            #$list insert end [file tail $file]
            #ReadTemplate $file $text
            incr ind
        }
    }
    foreach file [lsort [glob -nocomplain [file join $dir(work_tmpl) *.tex]]] {
        if {[file isdirectory $file] == 0} {
            $list insert end tmpl_$ind -text [file tail $file] \
            -image [Bitmap::get [file join $dir(img) tex_tmpl.gif]] \
            -data $file
            incr ind
            #$list insert end $file
            #$list insert end [file tail $file]
            #ReadTemplate $file $text
        }
    }
    #catch {cd ..}
}
proc ReadTemplateDescr {node} {
    global dir tmpl
    set text .template.frmCanv.f1.txtTemplate
    $text configure -state normal
    $text delete 0.0 end
    .template.frmCanv.f.list selection set $node
    
    #$w select set anchor [$w nearest $y]
    #set file [$w get [$w curselection]]
    set file [.template.frmCanv.f.list itemcget $node -data]
    
    set tmpl $file
    set fHandle [open $file]
    fconfigure $fHandle -encoding koi8-r
    while {[gets $fHandle line]>=0} {
        if {[string range [string trim $line] 0 0] != "%"} {return}    
        $text insert end "$line\n"
    }
    close $fHandle
    $text configure -state disable
}


