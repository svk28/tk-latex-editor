######################################################
#                TkTeXEditor
#        Distributed under GNU Public License
# Author: Sergey Kalinin (BanZaj) banzaj@lrn.ru
# Copyright (c) "CONERO lab", 2002, http://conero.lrn.ru
######################################################

######################################################
#                                                                                                                                                                             #
#  Working with trees (insert\update\deleted) nodes                                                               #
#                                                                                                                                                                             #
######################################################

## PROJECTS & FILE TREE ##
proc InsertTreeNode {file dirName} {
    global tree font dir
    set dot "_"
    set name [file rootname $file]
    set ext [string range [file extension $file] 1 end]
    regsub -all -nocase -- {/| |\.} $dirName $dot nodeEnd
    regsub -all -nocase -- { |\.} $name $dot name
    set subNode "$nodeEnd$dot$name$dot$ext"
    
    if {[$tree exists $subNode] == 1} {
        return $subNode
    }
    $tree insert end root $subNode -text $file \
    -data [file join $dirName $file] -open 1\
    -image [Bitmap::get [file join $dir(img) tex.gif]]\
    -font $font(normal)
    $tree configure -redraw 1
    return $subNode
    
}

## INSERT NODE INTO FILES TREE FOR INCLUDED FILES
proc InsertTreeSubNode {file rootNode dirName} {
    global tree font dir files
    set dot "_"
    set name [file rootname $file]
    set ext [string range [file extension $file] 1 end]
    if {$ext == ""} {
        #puts "$file - ��� ������������"
        set ext "tex"
        set file $file.$ext
        #puts $file
    }
    
    regsub -all -nocase -- {/| |\.} $dirName $dot nodeEnd
    regsub -all -nocase -- { |\.} $name $dot name
    set subNode "$nodeEnd$dot$name$dot$ext"
    #set subNode "$nodeEnd$dot$ext"
    #puts "file - $file\n"
    #puts "dirName - $dirName\n"
    #puts "rootNode - $rootNode\n"
    #puts "nodeEnd - $nodeEnd\n"
    #puts "name - $name\n"
    #puts "subnode - $subNode\n"
    if {[$tree exists $subNode] == 1} {
        if {[$tree parent $subNode] == "root"} {
            $tree delete $subNode
        } else {
            return
        }
    }
    $tree insert end $rootNode $subNode -text $file \
    -data [file join $dirName $file] -open 1\
    -image [Bitmap::get [file join $dir(img) tex.gif]]\
    -font $font(normal)
    $tree configure -redraw 1
    return $subNode
}
## DOCUMENT STRUCTURE TREE  ##
proc InsertNode {node parentNode text image lineNumber} {
    global treeStruct font dir
    set dot "_"
    $treeStruct insert end $parentNode $node -text [string trimleft $text "\*\{"] \
    -data " $lineNumber$dot$text" -open 1\
    -font $font(normal)\
    -image [Bitmap::get [file join $dir(img) $image.gif]]
    $treeStruct configure -redraw 1
    #return $subNode
}

## TREE ONE CLICKING PROCEDURE ##
proc TreeOneClick {node} {
    global dir nb tree activeFile status files
    $tree selection set $node
    set activeFile $node
    
    set file [$tree itemcget $node -data]
    #puts "TreeOneClick - $file";# debug
    if {[file exists $file] == 0} {
        set answer [tk_messageBox -message "$file [::msgcat::mc "File not found"]"\
        -type ok -icon warning -title [::msgcat::mc "Warning"]]
        case $answer {
            ok {return}
        }
    }
    
    $status(fileAttr) configure -text [FileAttr [$tree itemcget $node -data]]
    #$status(encode) configure -text "[lindex $files($activeFile) 2]"
    PageRaise $node
    puts "$node\nnodes - [$tree nodes $node]"
}

proc TreeStructOneClick {node} {
    global dir nb treeStruct activeFile status files ver release
    $treeStruct selection set $node
    set findString [$treeStruct itemcget $node -data]
    set star [string range $findString [expr [string first "_" $findString] + 1] [expr [string first "_" $findString] + 1]]
    set lineNumber [string range $findString 0 [expr [string first "_" $findString]-1]]
    set str [string range $node 0 [expr [string first "_" $node]-1]]
    if {$star == "\*"} {
        set findString [string range $findString [expr [string first "_" $findString]+2] end]
        set findString "$str\*$findString\}"
    } else {
        set findString [string range $findString [expr [string first "_" $findString]+1] end]
        set findString "$str\{$findString\}"
    }
    #$treeStruct selection get $node
    set text "$nb.f$activeFile.f.text"
    wm title . "TkLaTeXEditor $ver$release - [lindex $files($activeFile) 0]"
    
    FindProc $text $findString $node
    $status(pos) configure -text [$text index insert];# cursor position    
}

proc TreeDoubleClick {} {
    
}

proc NodeInsert {file} {
    global tree font dir files
    set dot "_"
    # �������
    set dirName [file dirname $file]
    # ��� �����
    set name [file rootname $file]
    # ����������
    set ext [string range [file extension $file] 1 end]
    if {$ext == ""} {
        #puts "$file - ��� ������������"
        set ext "tex"
        set file $file.$ext
        #puts $file
    }
    
    regsub -all -nocase -- {/| |\.} $dirName $dot nodeEnd
    regsub -all -nocase -- { |\.} $name $dot name
    
    if {} {
        
    }
    
    set subNode "$nodeEnd$dot$name$dot$ext"
    #set subNode "$nodeEnd$dot$ext"
    #puts "file - $file\n"
    #puts "dirName - $dirName\n"
    #puts "rootNode - $rootNode\n"
    #puts "nodeEnd - $nodeEnd\n"
    #puts "name - $name\n"
    #puts "subnode - $subNode\n"
    if {[$tree exists $subNode] == 1} {
        if {[$tree parent $subNode] == "root"} {
            $tree delete $subNode
        } else {
            return
        }
    }
    $tree insert end $rootNode $subNode -text $file \
    -data [file join $dirName $file] -open 1\
    -image [Bitmap::get [file join $dir(img) tex.gif]]\
    -font $font(normal)
    $tree configure -redraw 1
    return $subNode
    
}



