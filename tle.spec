Name:           tle
Version:        1.0.3
Release:        alt4
Summary:        Tk LaTeX Editor
Summary(ru_RU.KOI8-R): �������� ������ �� LaTeX
License:        GPL
Group:          Editors
Url:            http://conero.lrn.ru
BuildArch:      noarch
Source:         %name-%version-%release.tar.gz
Requires:   bwidget

%description
TkLaTeXeditor (tle) it's a powerfull LaTeX editor.
Code highlight, document structure navigator, project and file manager, spellchecking and much more

%description -l ru_RU.KOI8-R
TkLaTeXeditor (tle) ������������������� �������� ������ LaTeX. ������������ ���������� �������, ���������� ����������� ��������� ���������, ��������� �� ���������, �������� ����������, ������������� ������ ������������, ������������� ������� ��� ������� ����� � ������ ������.

%prep
%setup -n %name

%build

%install
mkdir -p $RPM_BUILD_ROOT{%_bindir,%_datadir/%name/img,%_datadir/%name/msg}
mkdir -p $RPM_BUILD_ROOT{%_datadir/%name/template,%_libdir/%name/command}
mkdir -p $RPM_BUILD_ROOT{%_libdir/%name/gui/toolbar,%_libdir/%name/modules,%_libdir/%name/conf}
mkdir -p $RPM_BUILD_ROOT{%_datadir/%name/img/default,%_datadir/%name/img/default/math}
mkdir -p $RPM_BUILD_ROOT{%_datadir/%name/img/stroke,%_datadir/%name/img/stroke/math}

install -p -m755 tle.tcl $RPM_BUILD_ROOT%_bindir/%name

install -p -m644 lib/modules/*.tcl $RPM_BUILD_ROOT%_libdir/%name/modules/
install -p -m644 lib/conf/*.* $RPM_BUILD_ROOT%_libdir/%name/conf/
install -p -m644 lib/command/*.* $RPM_BUILD_ROOT%_libdir/%name/command/
install -p -m644 lib/gui/toolbar/* $RPM_BUILD_ROOT%_libdir/%name/gui/toolbar/
install -p -m644 share/msg/*.* $RPM_BUILD_ROOT%_datadir/%name/msg/
install -p -m644 share/template/*.* $RPM_BUILD_ROOT%_datadir/%name/template

install -p -m644 share/img/default/*.* $RPM_BUILD_ROOT%_datadir/%name/img/default/
install -p -m644 share/img/default/math/*.* $RPM_BUILD_ROOT%_datadir/%name/img/default/math/
install -p -m644 share/img/stroke/*.* $RPM_BUILD_ROOT%_datadir/%name/img/stroke/
install -p -m644 share/img/stroke/math/*.* $RPM_BUILD_ROOT%_datadir/%name/img/stroke/math/

# Menu support
mkdir -p $RPM_BUILD_ROOT/usr/lib/menu

cat > $RPM_BUILD_ROOT/usr/lib/menu/%name << EOF
?package(%name): needs=x11 icon="tle.xpm" section="Applications/Editors" title=TkLaTeXeditor longtitle="Tk Latex Editor" command=tle
EOF

#mdk icons
install -d $RPM_BUILD_ROOT{%_iconsdir,%_iconsdir/large,%_iconsdir/mini}
install -p -m644 share/icons/%name.xpm $RPM_BUILD_ROOT%_iconsdir
install -p -m644 share/icons/large/%name.xpm $RPM_BUILD_ROOT%_iconsdir/large/
install -p -m644 share/icons/mini/%name.xpm $RPM_BUILD_ROOT%_iconsdir/mini/

%post
%update_menus

%postun
%clean_menus

%files
%doc INSTALL CHANGELOG TODO COPYING README THANKS KNOWBUG share/hlp/ru/*.*
%_bindir/%name
%_libdir/%name
%_datadir/%name
%_libdir/menu/%name
%_iconsdir/%name.xpm
%_liconsdir/%name.xpm
%_miconsdir/%name.xpm


%changelog
* Thu May 19 2005 Sergey Kalinin <banzaj@altlinux.ru> 1.0.3-alt4
- Fixed work with directory and files included brakespace into names

* Tue Sep 21 2004 Sergey Kalinin <banzaj@altlinux.ru> 1.0.3-alt3
- Image thumbnails support - stable

* Tue Sep 21 2004 Sergey Kalinin <banzaj@altlinux.ru> 1.0.3-alt2
- Fixed work with directory and files included whitespace into names

* Thu Sep 16 2004 Sergey Kalinin <banzaj@altlinux.ru> 1.0.3-alt1
- Added thumbnails support into text widget for \includegraphics - unstable
- Fixed preview bugs
- Fixed bug with differents beetwen system encoding and LaTeX encoding
- Added encode.lst where writing encoding equivalent
- Fixed scroling text window


