#!/usr/bin/wish
######################################################
#                Tk LaTeX Editor
#        Distributed under GNU Public License
# Author: Sergey Kalinin (BanZaj) banzaj@lrn.ru
# Home page: http//conero.lrn.ru
######################################################

set ver "1.0.3"
set release ""

package require BWidget
package require msgcat
package require Img

## DIRECTORY SETINGS ##
if [file exists tle.tcl] {
    set dir(root)     [pwd]
    set dir(lib)      [file join $dir(root) lib]
    set dir(share)    [file join $dir(root) share]
    set dir(hlp)      [file join $dir(share) hlp]
} else {
    set dir(root)     /usr
    set dir(lib)      [file join $dir(root) lib tle]
    set dir(share)    [file join $dir(root) share tle]
    set dir(hlp)      [file join $dir(root) share doc tle-$ver]
}
# �������
#set dir(root)     /home/svk/projects/tcl/tle
#set dir(lib)      [file join $dir(root) lib]
#set dir(share)    [file join $dir(root) share]
#set dir(hlp)      [file join $dir(share) hlp]

set dir(mod)          [file join $dir(lib) modules]
set dir(gui)          [file join $dir(lib) gui]
set dir(msg)          [file join $dir(share) msg]
set dir(tmpl)         [file join $dir(share) template]
set dir(cmd)          [file join $dir(lib) command]

if {$tcl_platform(platform) == "unix"} {
    set dir(work) [file join $env(HOME) .tle]
} elseif {$tcl_platform(platform) == "windows"} {
    if {[info exist env(HOMEDRIVE)] && [info exists env(HOMEPATH)]} {
        set dir(work) [file join $env(HOME) .tle]
        set dir(work) [file join $env(HOMEDRIVE)/$env(HOMEPATH) .tle]
    }
}
set dir(work_mod)     [file join $dir(work) modules]
set dir(work_gui)     [file join $dir(work) gui]
set dir(work_tmpl)    [file join $dir(work) template]
set dir(work_cmd)     [file join $dir(work) command]

if {[file exists $dir(work)] == 0} {file mkdir $dir(work)}

if {[file exists $dir(work_tmpl)] == 0} {file mkdir [file join $dir(work) template]}
if {[file exists [file join $dir(work_gui) toolbar]] == 0} {file mkdir [file join $dir(work) gui toolbar]}
if {[file exists [file join $dir(work_gui) hotkeys]] == 0} {file mkdir [file join $dir(work) gui hotkeys]}
if {[file exists $dir(work_cmd)] == 0} {file mkdir [file join $dir(work) command]}
if {[file exists $dir(work_mod)] == 0} {file mkdir [file join $dir(work) modules]}

## LOAD CONFIG ##
if {[file exists [file join $dir(work) tle.conf]] == 0} {
    file copy -- [file join $dir(lib) conf tle.conf] [file join $dir(work) tle.conf]
}
if {[file exists [file join $dir(work) encode.lst]] == 0} {
    file copy -- [file join $dir(lib) conf encode.lst] [file join $dir(work) encode.lst]
}

source [file join $dir(work) tle.conf]
if {[info exists theme] == 1} {
    set dir(img) [file join $dir(share) img $theme]
} else {
    set dir(img) [file join $dir(share) img]
}

foreach d {tmp doc} {
    if {[file exists $dir($d)] == 0} {file mkdir $dir($d)}
}

set dir(current) $dir(doc)

if {[info exists locale] == 1} {
    # old locale settings via $locale
    ::msgcat::mclocale $locale
    ::msgcat::mcload $dir(msg)
} else {
    # use system locale
    set sysenc [encoding system]
    encoding system utf-8
    ::msgcat::mcload $dir(msg)
    encoding system $sysenc
}

##################################
# new variable
#
if {[info exists thumb(show)] == 0} {
    set thumb(show) "No"
}
if {[info exists thumb(resolution)] == 0} {
    set thumb(resolution) "100x100"
}
if {[info exists thumb(format)] == 0} {
    set thumb(format) "png" 
}
####################################
source [file join $dir(mod) splashscreen.tcl]

foreach file [lsort [glob -nocomplain [file join $dir(mod) *.tcl]]] {
    if {[file tail $file] != "gui.tcl"  && [file tail $file] != "splashscreen.tcl"} {
        puts "Load module $file"
        source $file
    }
}
foreach file [lsort [glob -nocomplain [file join $dir(work_mod) *.tcl]]] {
    if {[file tail $file] != "gui.tcl"} {
        puts "Load module $file"
        source $file
    }
}
source [file join $dir(mod) gui.tcl]

GetPackage $dir(cmd)
GetPackage $dir(work_cmd)



## Open files was opened in previouse session ##
if [file exists [file join $dir(work) bookmarks]] {
    set f [open [file join $dir(work) bookmarks] "r"]
    while {[gets $f line]>=0} {
        scan $line "%s%s" file pos
        if [file exists $file] {
            EditFile $file
            set text "$nb.f[$nb raise].f.text"
            $text mark set insert $pos
            $text see $pos        
        }
    }
    close $f
}

set dir(current) $dir(doc)

## COMMAND LINE EDITING FILE LOUNCH ##
if [file isdirectory $argv] {
    puts "\'$argv\' is a directory"
} elseif [file exists $argv] {
    EditFile $argv
} else {
    puts stderr "File doesn't exist: $argv"
}



